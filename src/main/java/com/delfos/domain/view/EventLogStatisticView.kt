package com.delfos.domain.view

import com.delfos.domain.*
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.format.annotation.DateTimeFormat.ISO.DATE
import org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME
import java.util.*

data class EventLogStatisticView(
        @DateTimeFormat(iso = DATE)
        var date: Date? = null,

        @DateTimeFormat(iso = DATE_TIME)
        var dateHour: Date? = null,

        @JsonIgnoreProperties(value = ["enabled"])
        var component: Component? = null,

        var identifier: String? = null,

        var eventType: EventType? = null,

        @JsonIgnoreProperties(value = ["allRules", "delay", "retry"])
        var event: Event? = null,

        @JsonIgnoreProperties(value = ["limited"])
        var rule: Rule? = null,

        @JsonIgnoreProperties(value = ["system", "preLoad", "registryLog"])
        var message: Message? = null,

        var count: Long? = null
) {

    override fun toString(): String {
        return "EventLogStatisticView{" +
                "dateHour=" + dateHour +
                ", identifier='" + identifier + '\''.toString() +
                ", eventType=" + eventType +
                ", event=" + event +
                ", count=" + count +
                '}'.toString()
    }
}
