package com.delfos.domain

import com.delfos.domain.converter.EventTypeConverter
import com.eoscode.springapitools.data.domain.Find
import com.eoscode.springapitools.data.domain.Identifier
import com.eoscode.springapitools.data.domain.NoDelete
import com.fasterxml.jackson.annotation.JsonCreator
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.GenericGenerator
import org.hibernate.validator.constraints.Length
import org.springframework.data.annotation.Transient
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Entity
@Table(name = "event")
@NamedEntityGraphs(NamedEntityGraph(name = "Event.findDetailById",
        attributeNodes = [NamedAttributeNode("actions"), NamedAttributeNode("rules")]))
@NoDelete
@Find
data class Event(
        @Id
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        @GeneratedValue(generator = "uuid")
        @Column(name = "id")
        private var id: String? = null,

        @NotNull
        @Length(min = 4, max = 150)
        @Column(name = "name", length = 150, nullable = false)
        var name: String? = null,

        @Column(name = "description", length = 500)
        var description: String? = null,

        @Column(name = "state")
        var state: StateType? = null,

        @NotEmpty
        @ManyToMany
        @JoinTable(name = "event_rule", joinColumns = [JoinColumn(name = "event_id", referencedColumnName = "id")],
                inverseJoinColumns = [JoinColumn(name = "rule_id", referencedColumnName = "id")])
        @OrderBy(value = "name")
        var rules: MutableList<Rule>? = null,

        @ManyToMany(targetEntity = Action::class)
        @JoinTable(name = "event_action", joinColumns = [JoinColumn(name = "event_id", referencedColumnName = "id")],
                inverseJoinColumns = [JoinColumn(name = "action_id", referencedColumnName = "id")])
        var actions: MutableSet<Action>? = null,

        @Convert(converter = EventTypeConverter::class)
        @Enumerated(EnumType.ORDINAL)
        @Column(name = "type")
        var type: EventType? = null
) : Identifier<String> {

    @Column(name = "status", length = 1)
    @ColumnDefault("1")
    val status: Int? = null

    @Transient
    @Column(name = "allrules", length = 1)
    var allRules: Boolean = false

    @Transient
    @Column(name = "delay", length = 19)
    var delay: Long = 0

    @Transient
    @Column(name = "retry", length = 19)
    var retry: Long = 0

    @JsonCreator(mode = JsonCreator.Mode.DEFAULT)
    constructor() : this(null)

    constructor(id: String, name: String) : this(id) {
        this.name = name
    }

    constructor(id: String, name: String, type: EventType) : this(id, name) {
        this.type = type
    }

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    fun addRule(rule: Rule) {
        if (rules == null) {
            rules = ArrayList()
        }
        if (!rules!!.contains(rule)) {
            rules?.add(rule)
        }
    }

    fun removeRule(rule: Rule) {
        rules?.remove(rule)
    }

    fun addAction(action: Action) {
        if (actions == null) {
            actions = HashSet()
        }
        if (!actions!!.contains(action)) {
            actions?.add(action)
        }
    }

    fun removeAction(action: Action) {
        actions?.remove(action)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Event) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

}
