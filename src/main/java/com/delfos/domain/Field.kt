package com.delfos.domain

import com.eoscode.springapitools.data.domain.Find
import com.eoscode.springapitools.data.domain.FindAttribute
import com.eoscode.springapitools.data.domain.Identifier
import com.fasterxml.jackson.annotation.JsonCreator
import org.hibernate.annotations.DynamicUpdate
import org.hibernate.annotations.GenericGenerator

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
@DynamicUpdate
@Table(name = "field")
@NamedEntityGraph(name = "Field.findDetailById", attributeNodes = [NamedAttributeNode("message")])
@Find(/*ignoreAttributes = ["size", "isDecimalPoint", "precision", "indice"]*/)
data class Field(
        @Id
        @Column(name = "id")
        @GeneratedValue(generator = "uuid")
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        private var id: String? = null,

        @NotNull
        @Size(min = 2, max = 60)
        @Column(length = 60)
        var name: String?,

        @FindAttribute(ignore = true)
        @Column(name = "size")
        var size: Int = 0,

        @NotNull
        @Enumerated(EnumType.STRING)
        var type: FieldType?,

        @Column(name = "description", length = 500)
        var description: String? = null,

        @FindAttribute(ignore = true)
        @Column(name = "decimal_point")
        var isDecimalPoint: Boolean = false,

        @FindAttribute(ignore = true)
        @Column(name = "decimal_digit")
        var precision: Int = 0,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "message_id", nullable = false)
        var message: Message? = null,

        @FindAttribute(ignore = true)
        @Column(name = "indice")
        var indice: Int = 0,

        @Column(name = "value_format")
        var valueFormat: String? = null
) : Identifier<String> {

    @JsonCreator(mode = JsonCreator.Mode.DEFAULT)
    constructor() : this(null)

    constructor(id: String?) : this(id, "", 0, FieldType.STRING)

    @Column(name = "message_id", insertable = false, updatable = false)
    val messageId: String? = null
         get() = message?.id?.let { it } ?: field

    @Transient
    var parentField: Field? = null

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Field) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

}
