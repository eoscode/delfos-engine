package com.delfos.service

import com.delfos.domain.ContactGroup
import com.delfos.repository.ContactGroupRepository
import com.eoscode.springapitools.service.AbstractService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ContactGroupService : AbstractService<ContactGroupRepository, ContactGroup, String>() {

    @Autowired
    private val contactGroupRepository: ContactGroupRepository? = null

    override fun getRepository(): ContactGroupRepository? {
        return contactGroupRepository
    }

}
