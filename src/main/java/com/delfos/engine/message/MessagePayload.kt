package com.delfos.engine.message

import com.fasterxml.jackson.annotation.JsonRawValue
import com.fasterxml.jackson.databind.JsonNode

data class MessagePayload(
        var identifier: String? = null,
        var name: String? = null,

        @JsonRawValue
        var content: JsonNode? = null
)
