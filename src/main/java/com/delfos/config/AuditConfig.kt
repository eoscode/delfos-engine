package com.delfos.config

import com.eoscode.springapitools.security.Auth
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.domain.AuditorAware
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.security.core.context.SecurityContextHolder
import java.util.*

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
class AuditConfig {

    @Bean
    fun auditorAware(): AuditorAware<String> {
        return SecurityAuditor()
    }

    @Bean
    fun createAuditingListener(): AuditingEntityListener {
        return AuditingEntityListener()
    }

    class SecurityAuditor : AuditorAware<String> {
        override fun getCurrentAuditor(): Optional<String> {
            if (SecurityContextHolder.getContext().authentication == null) {
                return Optional.empty()
            }

            val authObject = SecurityContextHolder.getContext().authentication.principal
            if (authObject is Auth<*>) {
                val auth = SecurityContextHolder.getContext().authentication.principal as Auth<*>
                return Optional.ofNullable(auth.username)
            }
            return Optional.empty()
        }
    }

}
