package com.delfos.service

import com.delfos.domain.ActionType
import com.delfos.repository.ActionTypeParameterRepository
import com.delfos.repository.ActionTypeRepository
import com.eoscode.springapitools.service.AbstractService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ActionTypeService @Autowired
constructor(private val actionTypeRepository: ActionTypeRepository,
            private val actionTypeParameterRepository: ActionTypeParameterRepository) : AbstractService<ActionTypeRepository, ActionType, String>() {

    override fun getRepository(): ActionTypeRepository {
        return actionTypeRepository
    }

    @Transactional
    override fun save(entity: ActionType): ActionType {

        if (entity.id != null) {
            val actionTypeOld = findDetailById(entity.id)

            actionTypeOld?.parameters?.apply {
                if (size > 0) {
                    removeAll(entity.parameters!!)
                    forEach { parameter -> actionTypeParameterRepository.deleteById(parameter.id!!) }
                }
            }
        }

        entity.parameters?.filter { it.actionType == null }?.forEach { it.actionType = entity }

        return super.save(entity)
    }

}
