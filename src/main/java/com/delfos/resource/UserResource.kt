package com.delfos.resource

import com.delfos.domain.User
import com.delfos.service.UserService
import com.eoscode.springapitools.resource.AbstractResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/users")
class UserResource @Autowired
constructor(private val userService: UserService) : AbstractResource<UserService, User, String>() {

    override fun getService(): UserService {
        return userService
    }

    @PostMapping("/{id}/change/password")
    fun changePassword(@PathVariable("id") id: String, @RequestBody model: Map<Any, Any>): User {
        return userService.changePassword(id, model["password"] as String)
    }

}
