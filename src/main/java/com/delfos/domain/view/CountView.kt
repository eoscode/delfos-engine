package com.delfos.domain.view

data class CountView(
        val viewId: String,
        val count: Long?,
        val groups: ArrayList<String>?
){
    constructor(viewId: String, count: Long?) : this(viewId, count, null)
}
