package com.delfos.repository

import com.delfos.domain.ActionType
import com.eoscode.springapitools.data.repository.Repository

import java.util.Optional

@org.springframework.stereotype.Repository
interface ActionTypeRepository : Repository<ActionType, String> {

    fun findByNameStartingWith(name: String): Optional<ActionType>

}
