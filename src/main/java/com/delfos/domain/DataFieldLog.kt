package com.delfos.domain

import org.springframework.data.mongodb.core.index.Indexed

import java.io.Serializable

open class DataFieldLog : Serializable {

    @Indexed
    open var fieldId: String? = null

    @Indexed
    open var name: String? = null

    open var type: FieldType? = null

    @Indexed
    open var value: Any? = null
}
