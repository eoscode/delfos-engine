package com.delfos.config

import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@Configuration
@EnableJpaRepositories(basePackages = ["com.delfos.repository"])
@EnableMongoRepositories(basePackages = ["com.delfos.repository.mongo"])
class StandardConfig
