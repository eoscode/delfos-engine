package com.delfos.config

import com.delfos.service.DBServices
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.data.jpa.repository.config.EnableJpaAuditing

import java.text.ParseException


@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@Profile("test")
class TestConfig {

    @Autowired
    private val dbServices: DBServices? = null

    @Bean
    @Throws(ParseException::class)
    fun instantiateDatabase(): Boolean {
        dbServices!!.instantiateTestDatabase()
        return true
    }


}


