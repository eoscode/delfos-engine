package com.delfos.domain

import com.eoscode.springapitools.data.domain.Identifier
import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import org.hibernate.annotations.GenericGenerator
import org.hibernate.validator.constraints.Length
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Entity
@Table(name = "action")
@NamedEntityGraphs(NamedEntityGraph(name = "Action.findDetailById",
        attributeNodes = [NamedAttributeNode("parameters"),
            NamedAttributeNode(value = "parameters", subgraph = "parameters.type")],
        subgraphs = [NamedSubgraph(name = "parameter.type", attributeNodes = [NamedAttributeNode("type")])]))
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id")
data class Action (
        @Id
        @Column(name = "id")
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        @GeneratedValue(generator = "uuid")
        private var id: String? = null,

        @NotEmpty
        @Length(min = 2, max = 150)
        @Column(name = "name", nullable = false, length = 150)
        var name: String? = null,

        @Column(name = "description", length = 500)
        var description: String? = null,

        @Column(name = "action_type_id", insertable = false, updatable = false)
        private val actionTypeId: String? = null,

        @NotNull
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "action_type_id", nullable = false)
        var actionType: ActionType? = null,

        @JsonIgnoreProperties("action", "actionType")
        @OneToMany(mappedBy = "action", cascade = [CascadeType.PERSIST,
            CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.DETACH,
            CascadeType.MERGE], targetEntity = ActionParameter::class, orphanRemoval = true)
        var parameters: MutableList<ActionParameter>? = null

) : Identifier<String> {

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    fun getActionTypeId(): String? {
        if (actionTypeId != null) {
            return actionTypeId
        } else if (actionType != null) {
            return actionType!!.id
        }
        return null
    }

    fun getParameterKey(actionTypeParameter: ActionTypeParameter): ActionParameter? {
        return if (parameters != null) {
            parameters!!.stream().filter { parameter -> parameter.type == actionTypeParameter }
                    .findFirst().orElse(null)
        } else null
    }

    fun addParameter(parameter: ActionParameter) {
        if (parameters == null) {
            parameters = ArrayList()
        }
        parameter.action = this
        parameters!!.add(parameter)
    }

    fun removeParameter(parameter: ActionParameter) {
        if (parameters != null) {
            parameters!!.remove(parameter)
        }
    }

}
