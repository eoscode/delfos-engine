package com.delfos.service

import com.delfos.domain.Contact
import com.delfos.repository.ContactRepository
import com.eoscode.springapitools.service.AbstractService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ContactService : AbstractService<ContactRepository, Contact, String>() {

    @Autowired
    private val contactRepository: ContactRepository? = null

    override fun getRepository(): ContactRepository? {
        return contactRepository
    }

}
