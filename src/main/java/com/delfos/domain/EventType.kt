package com.delfos.domain


enum class EventType constructor(var value: Int) {
    OK(0),
    //value = 0
    WARNING(1),
    //value = 1
    INFORMATION(2),
    //value = 2
    ERROR(3),
    // value = 3
    FATAL(4)
}
 
