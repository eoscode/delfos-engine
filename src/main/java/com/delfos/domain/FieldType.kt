package com.delfos.domain

enum class FieldType(val value: Int) {
    STRING(0),
    NUMBER(1),
    BOOLEAN(2),
    DATE(3);
}
