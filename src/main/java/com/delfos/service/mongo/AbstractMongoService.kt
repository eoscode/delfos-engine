package com.delfos.service.mongo

import com.eoscode.springapitools.data.domain.Identifier
import com.eoscode.springapitools.service.exceptions.EntityNotFoundException
import com.eoscode.springapitools.util.NullAwareBeanUtilsBean
import org.apache.commons.logging.LogFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.transaction.annotation.Transactional
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

abstract class AbstractMongoService<Repository : MongoRepository<Entity, ID>, Entity, ID> {

    protected val log = LogFactory.getLog(this.javaClass)

    abstract val repository: Repository

    private val repositoryType: Type
    private val entityType: Type
    private val identifierType: Type

    init {
        val type = javaClass.genericSuperclass
        val pType = type as ParameterizedType

        repositoryType = pType.actualTypeArguments[0]
        entityType = pType.actualTypeArguments[1]
        identifierType = pType.actualTypeArguments[2]
    }

    @Transactional
    open fun save(entity: Entity): Entity {
        return repository.save(entity)
    }

    @Transactional
    open fun update(entity: Entity): Entity {

        var entityOld: Entity? = null
        if (entity is Identifier<*>) {
            val id = (entity as Identifier<ID>).id

            entityOld = id?.let {
                findById(it)
            }

            entityOld?.apply {
                try {
                    NullAwareBeanUtilsBean.getInstance().copyProperties(this, entity)
                } catch (e: IllegalAccessException) {
                    e.printStackTrace()
                } catch (e: InvocationTargetException) {
                    e.printStackTrace()
                }
            }

        }

        return repository.save(entityOld!!)
    }

    @Throws(EntityNotFoundException::class)
    fun findById(id: ID): Entity {
        val entity: Entity? = repository.findByIdOrNull(id)
        if (entity != null) {
            return entity
        } else {
            throw EntityNotFoundException(
                    "Object not found! Id: " + id + ", Type: " + entityType.typeName)
        }
    }

    fun existsById(id: ID): Boolean {
        return repository.existsById(id)
    }

    @Transactional
    open fun deleteById(id: ID) {
        repository.deleteById(id)
    }

    @Transactional
    open fun delete(entity: Entity) {
        repository.delete(entity)
    }

    fun findPages(pageable: Pageable): Page<Entity> {
        return repository.findAll(pageable)
    }

    fun findAll(): List<Entity> {
        return repository.findAll()
    }

}
