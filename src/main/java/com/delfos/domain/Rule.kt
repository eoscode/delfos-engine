package com.delfos.domain

import com.eoscode.springapitools.data.domain.Find
import com.eoscode.springapitools.data.domain.Identifier
import com.eoscode.springapitools.data.domain.NoDelete
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.DynamicUpdate
import org.hibernate.annotations.GenericGenerator
import org.hibernate.validator.constraints.Length
import org.springframework.data.annotation.Transient
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Entity
@Table(name = "rule")
@DynamicUpdate
@EntityListeners(AuditingEntityListener::class)
@NamedEntityGraphs(NamedEntityGraph(name = "Rule.findDetailById", attributeNodes = [NamedAttributeNode("conditions")]))
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id")
@NoDelete
@Find
data class Rule(
        @Id
        @Column(name = "id")
        @GeneratedValue(generator = "uuid")
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        private var id: String? = null,

        @Column(name = "state")
        var state: StateType? = null,

        @NotNull
        @Length(min = 4, max = 150)
        @Column(name = "name", length = 150, nullable = false)
        var name: String? = null,

        @Column(length = 500)
        var description: String? = null,

        @NotEmpty
        @JsonIgnoreProperties(value = ["rule", "parent", "message"])
        @OneToMany(mappedBy = "rule", cascade = [CascadeType.PERSIST, CascadeType.REMOVE,
            CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE])
        @OrderBy(value = "order")
        var conditions: MutableList<Condition>? = null
) : Auditable(), Identifier<String> {

    @Column(name = "STATUS", length = 1)
    @ColumnDefault("1")
    val status: Int? = null

    @Transient
    @Column(name = "limited", length = 1)
    var isLimited: Boolean = false

    @JsonCreator(mode = JsonCreator.Mode.DEFAULT)
    constructor() : this(null)

    constructor(id: String, name: String) : this(id) {
        this.name = name
    }

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    fun addCondition(ruleCondition: Condition) {
        if (conditions == null) {
            conditions = ArrayList()
        }
        ruleCondition.rule = this

        if (ruleCondition.order == null) {
            val order = conditions?.size ?: 0
            ruleCondition.order = order
        }
        conditions!!.add(ruleCondition)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Rule) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

}
