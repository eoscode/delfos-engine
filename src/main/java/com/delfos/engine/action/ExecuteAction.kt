package com.delfos.engine.action

import com.delfos.domain.Event
import com.delfos.domain.Rule
import com.delfos.engine.message.MessageModel

abstract class ExecuteAction(val ocurrence: String,
                             val event: Event,
                             val rule: Rule,
                             val messageModel: MessageModel,
                             val parameters: Map<String, Any>
) : Runnable {

    protected abstract fun execute()

    override fun run() {
        execute()
    }

}
