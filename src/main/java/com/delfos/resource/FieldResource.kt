package com.delfos.resource

import com.delfos.domain.Field
import com.delfos.service.FieldService
import com.eoscode.springapitools.resource.AbstractResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/fields")
class FieldResource : AbstractResource<FieldService, Field, String>() {

    @Autowired
    private lateinit var fieldService: FieldService

    override fun getService(): FieldService {
        return fieldService
    }

}
