package com.delfos.repository

import com.delfos.domain.ActionTypeParameter
import com.eoscode.springapitools.data.repository.Repository

@org.springframework.stereotype.Repository
interface ActionTypeParameterRepository : Repository<ActionTypeParameter, String>
