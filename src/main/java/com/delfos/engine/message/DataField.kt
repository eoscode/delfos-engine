package com.delfos.engine.message

import com.delfos.domain.Field
import com.delfos.domain.FieldType

data class DataField(
    val field: Field,
    val value: String,
    val valueObject: Any
) {
    val description: String?
        get() = this.field.description

    val indice: Int
        get() = this.field.indice

    val name: String?
        get() = this.field.name

    val precision: Int
        get() = this.field.precision

    val size: Int
        get() = this.field.size

    val type: FieldType?
        get() = this.field.type

    val isDecimalPoint: Boolean
        get() = this.field.isDecimalPoint
}
