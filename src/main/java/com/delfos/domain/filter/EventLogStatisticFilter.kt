package com.delfos.domain.filter

data class EventLogStatisticFilter(
        var filters: EventLogFilter? = null,
        var groupBy: List<String>? = null,
        var sortBy: List<String>? = null,
        var limit: Long? = null
)
