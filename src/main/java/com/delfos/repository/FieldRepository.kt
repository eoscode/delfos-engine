package com.delfos.repository

import com.delfos.domain.Field
import com.eoscode.springapitools.data.repository.Repository

@org.springframework.stereotype.Repository
interface FieldRepository : Repository<Field, String>
