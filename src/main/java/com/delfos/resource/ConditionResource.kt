package com.delfos.resource

import com.delfos.domain.Condition
import com.delfos.service.ConditionService
import com.eoscode.springapitools.resource.AbstractResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/conditions")
class ConditionResource : AbstractResource<ConditionService, Condition, String>() {

    @Autowired
    private lateinit var conditionService: ConditionService

    override fun getService(): ConditionService {
        return conditionService
    }
}
