package com.delfos.domain

import javax.persistence.DiscriminatorValue
import javax.persistence.Entity

@Entity
@DiscriminatorValue(value = "0")
class GroupCondition(id: String?) : Condition(id) {

    override fun supportedOperators(): List<String>? {
        return null
    }

    override fun type(): ConditionType? {
        return ConditionType.GROUP
    }

}
