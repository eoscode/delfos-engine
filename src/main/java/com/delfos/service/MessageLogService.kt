package com.delfos.service


import com.delfos.domain.MessageLog
import com.delfos.domain.filter.MessageLogFilter
import com.delfos.domain.filter.MessageLogStatisticFilter
import com.delfos.domain.view.MessageLogStatisticView
import com.delfos.repository.mongo.MessageLogRepository
import com.delfos.service.mongo.AbstractMongoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation.*
import org.springframework.data.mongodb.core.aggregation.AggregationOperation
import org.springframework.data.mongodb.core.aggregation.DateOperators
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.repository.support.PageableExecutionUtils
import org.springframework.stereotype.Service
import java.util.*

@Service
class MessageLogService @Autowired
constructor(override val repository: MessageLogRepository,
            private val mongoTemplate: MongoTemplate) : AbstractMongoService<MessageLogRepository, MessageLog, String>() {

    fun find(filterBy: MessageLogFilter, pageable: Pageable): Page<MessageLog> {

        val criterias = buildCriteriaList(filterBy)

        val criteria = Criteria()
        if (criterias.isNotEmpty()) {
            criteria.andOperator(*criterias.toTypedArray())
        }
        val query = Query(criteria).with(pageable).with(Sort.by(Sort.Direction.DESC, "dateHour"))
        query.fields()
                .include("_id")
                .include("dateHour")
                .include("identifier")
                .include("message")
                .include("messageId")
                .include("component")
                .include("componentId")

        var list: List<MessageLog> = ArrayList()
        try {
            list = mongoTemplate.find(query, MessageLog::class.java)
        } catch (e: Exception) {
            log.error("find message log:", e)
        }

        return PageableExecutionUtils.getPage(
                list,
                pageable
        ) { mongoTemplate.count(query, MessageLog::class.java) }
    }

    fun statisticQuery(statisticBy: MessageLogStatisticFilter): List<MessageLogStatisticView> {

        val operations = ArrayList<AggregationOperation>()
        val criterias = buildCriteriaList(statisticBy.filters!!)

        if (!criterias.isNullOrEmpty()) {
            val criteria = Criteria()
            operations.add(match(criteria.andOperator(*criterias.toTypedArray())))
        }

        if (!statisticBy.groupBy.isNullOrEmpty()) {
            statisticBy.groupBy?.apply {
                val groups = toTypedArray()
                val projections = toTypedArray()

                if (groups.isNotEmpty()) {

                    var projectionOperation = project()

                    for (field in projections) {
                        if (field.equals("date", ignoreCase = true)) {
                            projectionOperation = projectionOperation.and(DateOperators.DateToString.dateOf("dateHour")
                                    .toString("%Y-%m-%d")).`as`("dateWithOutHour")
                        } else if (field.equals("dateHour", ignoreCase = true)) {
                            projectionOperation = projectionOperation.and(DateOperators.DateToString.dateOf("dateHour")
                                    .toString("%Y-%m-%d %H")).`as`("dateWithHour")
                        } else {
                            projectionOperation = projectionOperation.and(field).`as`(field)
                        }
                    }
                    operations.add(projectionOperation)

                    projectionOperation = project()
                    for (field in projections) {
                        if (field.equals("date", ignoreCase = true)) {
                            projectionOperation = projectionOperation.and(DateOperators.DateFromString.fromStringOf("dateWithOutHour")
                                    .withFormat("%Y-%m-%d")).`as`("date")
                        } else if (field.equals("dateHour", ignoreCase = true)) {
                            projectionOperation = projectionOperation.and(DateOperators.DateFromString.fromStringOf("dateWithHour")
                                    .withFormat("%Y-%m-%d %H")).`as`("dateHour")
                        } else {
                            projectionOperation = projectionOperation.and(field).`as`(field)
                        }
                    }
                    operations.add(projectionOperation)

                    operations.add(group(*groups).count().`as`("count"))

                    if (projections.size > 1) {
                        operations.add(project("count").andInclude(*projections))
                    } else if (projections.size == 1) {
                        operations.add(project("count").and(projections[0]).previousOperation())
                    }
                }
            }
        } else {
            operations.add(count().`as`("count"))
        }

        operations.add(sort(Sort.Direction.DESC, "count"))

        statisticBy.limit?.let { operations.add(limit(it)) }

        val agg = newAggregation(operations)
        val groupResults = mongoTemplate.aggregate(agg, MessageLog::class.java, MessageLogStatisticView::class.java)
        return groupResults.mappedResults
    }

    fun statisticQueryByDate(
            dateHour: Date?,
            dateHourEnd: Date?,
            groups: List<String>?): List<MessageLogStatisticView> {

        val aggregateBy = MessageLogStatisticFilter()
        groups?.let { aggregateBy.groupBy = it.toList() }

        val filter = MessageLogFilter()
        filter.dateHour = dateHour?.let { it } ?: Date()
        filter.dateHourEnd = dateHourEnd

        aggregateBy.filters = filter

        return statisticQuery(aggregateBy)

    }

    private fun buildCriteriaList(filterBy: MessageLogFilter): List<Criteria> {

        val criterias = ArrayList<Criteria>()

        if (filterBy.componentId != null) {
            criterias.add(Criteria.where("componentId").`is`(filterBy.componentId))
        }

        if (filterBy.identifier != null) {
            criterias.add(Criteria.where("identifier").`is`(filterBy.identifier))
        }

        if (filterBy.messageId != null) {
            criterias.add(Criteria.where("messageId").`is`(filterBy.messageId))
        }

        if (filterBy.dateHour != null || filterBy.dateHourEnd != null) {
            criterias.add(Util.dateToBetweenCriteria("dateHour",
                    filterBy.dateHour, filterBy.dateHourEnd))
        }

        if (!filterBy.dataFields.isNullOrEmpty()) {
            filterBy.dataFields.forEach { dataFieldLog ->
                val elemMatch = Criteria()
                elemMatch.andOperator(Util.dataFieldToCriteria(dataFieldLog))

                criterias.add(Criteria.where("dataFields")
                        .elemMatch(elemMatch))
            }
        }

        return criterias
    }

}
