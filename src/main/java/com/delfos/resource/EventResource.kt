package com.delfos.resource

import com.delfos.domain.Event
import com.delfos.service.EventService
import com.eoscode.springapitools.resource.AbstractResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/events")
class EventResource : AbstractResource<EventService, Event, String>() {

    @Autowired
    private val eventService: EventService? = null

    override fun getService(): EventService? {
        return eventService
    }

    @GetMapping("/find/name")
    fun search(@RequestParam("name") name: String): ResponseEntity<List<Event>> {
        val list = service!!.findByNameOrDescription(name)
        return ResponseEntity.ok(list)
    }

}
