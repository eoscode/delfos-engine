package com.delfos.engine.util

import com.delfos.domain.Field
import com.delfos.domain.FieldType
import com.fasterxml.jackson.databind.JsonNode

import java.math.BigDecimal
import java.text.SimpleDateFormat


object FieldObjectUtil {

    fun getString(field: Field, value: String): String? {
        try {
            when (field.type) {
                FieldType.NUMBER -> return if (field.isDecimalPoint) {
                    var bigDecimal = BigDecimal(value)
                    bigDecimal = bigDecimal.movePointLeft(field.precision)
                    bigDecimal.toString()
                } else {
                    value
                }
                FieldType.BOOLEAN -> return if (java.lang.Boolean.getBoolean(value)) {
                    "1"
                } else {
                    "0"
                }
                FieldType.STRING -> return value
                FieldType.DATE -> return value
            }
        } catch (e: Exception) {
            return null
        }
        return null
    }

    fun getObject(field: Field, jsonNode: JsonNode): Any? {
        try {
            when (field.type) {
                FieldType.NUMBER -> return if (jsonNode.isDouble) {
                    jsonNode.doubleValue()
                } else {
                    jsonNode.longValue()
                }
                FieldType.BOOLEAN -> return jsonNode.booleanValue()
                FieldType.STRING -> return jsonNode.asText()
                FieldType.DATE -> {
                    //HHmmss
                    var format: String? = "yyyyMMdd"
                    if (field.valueFormat != null && "" != field.valueFormat) {
                        format = field.valueFormat
                    }
                    val dateFormat = SimpleDateFormat(format!!)
                    return dateFormat.parse(jsonNode.asText())
                }
            }
        } catch (e: Exception) {
            return null
        }
        return null
    }

    fun getObject(jsonNode: JsonNode): Any? {
        try {
            when {
                jsonNode.isDouble -> return jsonNode.doubleValue()
                jsonNode.isNumber -> return jsonNode.longValue()
                jsonNode.isDouble -> return jsonNode.booleanValue()
                jsonNode.isTextual -> return jsonNode.asText()
            }
        } catch (e: Exception) {
            return null
        }

        return null
    }
}
