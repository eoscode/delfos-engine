package com.delfos.domain

import com.eoscode.springapitools.data.domain.Find
import com.eoscode.springapitools.data.domain.Identifier
import com.eoscode.springapitools.data.domain.NoDelete
import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.DynamicUpdate
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

@DynamicUpdate
@Entity
@Table(name = "user")
@NoDelete
@Find
data class User(
        @Id
        @Column(name = "id")
        @GeneratedValue(generator = "uuid")
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        private var id: String? = null,

        @NotEmpty
        @Size(min = 2, max = 150)
        @Column(name = "name", nullable = false, length = 150)
        var name: String? = null,

        @NotEmpty
        @Size(min = 2, max = 200)
        @Column(name = "username", nullable = false, unique = true, length = 200)
        var username: String? = null,

        @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
        @NotEmpty
        @Column(name = "password", nullable = false, length = 256)
        var password: String? = null
): Identifier<String> {

    @Column(name = "status", length = 1, nullable = false)
    @ColumnDefault("1")
    val status: Int? = null

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is User) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

}
