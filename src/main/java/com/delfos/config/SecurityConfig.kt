package com.delfos.config

import com.delfos.service.UserService
import com.eoscode.springapitools.filter.HeaderExposureFilter
import com.eoscode.springapitools.security.jwt.JWTAuthenticationFilter
import com.eoscode.springapitools.security.jwt.JWTAuthorizationFilter
import com.eoscode.springapitools.security.jwt.JWTManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfig : WebSecurityConfigurerAdapter() {

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var env: Environment

    @Autowired
    private lateinit var jwtManager: JWTManager

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {

        if (listOf(*env.activeProfiles).contains("test")) {
            http.headers().frameOptions().disable()
        }

        http.cors().and().csrf().disable()
        http.authorizeRequests()
                .antMatchers("/login*").anonymous()
                //.antMatchers(HttpMethod.GET, API_MATCHERS).permitAll()
                .antMatchers(HttpMethod.POST, *PUBLIC_MATCHERS_POST).permitAll()
                .antMatchers(*PUBLIC_MATCHERS).permitAll()
                .antMatchers("/api/analyzer/**").permitAll()
                .antMatchers("/api/**").authenticated()

                .anyRequest().permitAll()

        http.addFilter(JWTAuthenticationFilter(authenticationManager(), jwtManager))
        http.addFilter(JWTAuthorizationFilter(authenticationManager(), jwtManager, userService))
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }

    @Throws(Exception::class)
    public override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService<UserService>(userService).passwordEncoder(bCryptPasswordEncoder())
    }

    @Throws(Exception::class)
    override fun configure(web: WebSecurity) {
        web.ignoring().antMatchers("/front/**", "/index.html")
    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val configuration = CorsConfiguration().applyPermitDefaultValues()
        configuration.allowedMethods = listOf("POST", "GET", "PUT", "DELETE", "PATCH", "OPTIONS")
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }

    @Bean
    fun bCryptPasswordEncoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun jwtManager(): JWTManager {
        return JWTManager()
    }


    @Bean
    fun headerExposureFilter(): HeaderExposureFilter {
        return HeaderExposureFilter()
    }

    companion object {

        private val PUBLIC_MATCHERS = arrayOf("/h2-console/**", "/analyzer/**")

        private val PUBLIC_MATCHERS_POST = arrayOf("/auth/forgot/**")
    }

}
