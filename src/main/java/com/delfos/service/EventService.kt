package com.delfos.service

import com.delfos.domain.Event
import com.delfos.domain.Rule
import com.delfos.engine.rules.ValidationRuleManager
import com.delfos.repository.EventRepository
import com.eoscode.springapitools.service.AbstractService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class EventService : AbstractService<EventRepository, Event, String>() {

    @Autowired
    private val eventRepository: EventRepository? = null

    @Autowired
    private val ruleService: RuleService? = null

    @Autowired
    private val validationRuleManager: ValidationRuleManager? = null

    override fun getRepository(): EventRepository? {
        return eventRepository
    }

    private fun prepareRules(event: Event) {
        event.rules = event.rules?.map {
            if (it.id != null) {
                ruleService!!.findById(it.id)
            } else {
                it
            }
        }?.toMutableList()
    }

    @Transactional
    override fun save(entity: Event): Event {
        prepareRules(entity)
        return super.save(entity)
    }

    fun findByEnabledAndRule(rule: Rule): List<Event> {
        return repository!!.findByEnabledAndRule(rule.id!!)
                .orElse(emptyList())
    }

    fun findByNameOrDescription(term: String): List<Event> {
        return repository!!.findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCaseOrderByName("%$term%", term)
    }

}
