package com.delfos.resource

import com.delfos.domain.User
import com.delfos.service.UserService
import com.eoscode.springapitools.security.jwt.JWTAuthenticationResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping("/api/auth")
class AuthResource @Autowired
constructor(private val userService: UserService) : JWTAuthenticationResource() {

    @PostMapping("/refresh/token")
    override fun refreshToken(response: HttpServletResponse): ResponseEntity<String> {
        return super.refreshToken(response)
    }

    @PostMapping("/change/password")
    fun changePassword(@RequestBody changePassword: ChangePassword): User {
        return userService.changePasswordCurrentUser(changePassword.password, changePassword.newPassword)
    }

    data class ChangePassword(var password: String,
                              var newPassword: String)

}
