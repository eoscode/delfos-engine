package com.delfos.engine.message

import java.util.EventListener

interface MessageListener : EventListener {

    fun notificationPerformed(messageModel: MessageModel)

}
 
