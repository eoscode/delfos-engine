package com.delfos.domain

import com.eoscode.springapitools.data.domain.Find
import com.eoscode.springapitools.data.domain.FindAttribute
import com.eoscode.springapitools.data.domain.Identifier
import com.eoscode.springapitools.data.domain.NoDelete
import com.fasterxml.jackson.annotation.*
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.DynamicUpdate
import org.hibernate.annotations.GenericGenerator
import org.springframework.data.annotation.Transient
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import javax.persistence.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size


@Entity
@DynamicUpdate
@Table(name = "message")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id")
@NamedEntityGraphs(NamedEntityGraph(name = "Message.findDetailById",
        attributeNodes = [NamedAttributeNode("messageGroup"), NamedAttributeNode("fields")]))
@EntityListeners(AuditingEntityListener::class)
@NoDelete
@Find
data class Message(
        @Id
        @Column(name = "id")
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        @GeneratedValue(generator = "uuid")
        private var id: String? = null,

        @NotEmpty
        @Size(min = 2, max = 60)
        @Column(name = "name", length = 60, unique = true)
        var name: String? = null,

        @Column(length = 1000)
        var description: String? = null,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "message_group_id")
        var messageGroup: MessageGroup? = null,

        @JsonIgnoreProperties("message")
        @NotEmpty
        @OneToMany(mappedBy = "message", fetch = FetchType.LAZY,
                cascade = [CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE])
        @OrderBy(value = "indice")
        var fields: MutableList<Field>? = null,

        @FindAttribute(ignore = true)
        @Column(name = "registry_log", length = 1)
        var registryLog: Boolean = false
) : Auditable(), Identifier<String> {

    @Column(name = "STATUS", length = 1)
    @ColumnDefault("1")
    val status: Int? = null

    @Column(name = "message_group_id", insertable = false, updatable = false)
    val messageGroupId: String? = null
        get() {
            return messageGroup?.id?.let { it } ?: field
        }

    @Transient
    @Column(name = "system_message", length = 1)
    var system: Boolean = false

    @Transient
    @Column(name = "preload", length = 1)
    var preLoad: Boolean = false

    @JsonCreator(mode = JsonCreator.Mode.DEFAULT)
    constructor(): this(null)

    constructor(id: String, name: String): this(id) {
        this.id = id
        this.name = name
    }

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    fun isEmpty(): Boolean {
        return fields?.isEmpty() ?: false
    }

    @JsonAnySetter
    fun addField(field: Field) {
        if (fields == null) {
            fields = ArrayList()
        }

        field.indice = fields!!.size
        field.message = this
        fields!!.add(field)
    }

    fun getField(key: String): Field? {
        return fields?.find { it.name.equals(key, ignoreCase = true) }
    }

    fun removeField(field: Field) {
        if (fields != null) {
            fields!!.remove(field)
        }
    }

    fun size(): Int {
        return if (fields != null) fields!!.size else 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Message) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

}
