package com.delfos.service

import com.delfos.domain.Message
import com.delfos.repository.MessageRepository
import com.eoscode.springapitools.service.AbstractService
import com.eoscode.springapitools.service.exceptions.EntityNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service("messageService")
class MessageService @Autowired
constructor(private val messageRepository: MessageRepository,
            private val fieldService: FieldService) : AbstractService<MessageRepository, Message, String>() {

    override fun getRepository(): MessageRepository {
        return messageRepository
    }

    fun findByName(name: String): Message {

        return repository.findByName(name).orElseThrow { EntityNotFoundException("message not found! name: $name") }

    }

    @Transactional
    override fun save(entity: Message): Message {

        if (entity.id != null) {
            val entityOld = findDetailById(entity.id)
            entityOld?.fields?.apply {
                if (size > 0) {
                    removeAll(entity.fields?.let { it }.orEmpty())
                    forEach { parameter -> fieldService.deleteById(parameter.id!!) }
                }
            }

        }

        entity.fields?.filter { it.message == null }?.forEach { it.message = entity }

        return super.save(entity)
    }

    fun findByNameOrDescription(term: String): List<Message> {
        return repository.findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCaseOrderByName("%$term%", term)
    }

}
