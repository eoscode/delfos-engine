package com.delfos.domain

enum class StateType (val status: Int) {

    DISABLED(0),
    ENABLED(1),
    DELETED(2);

}
