package com.delfos.resource.util

import com.delfos.domain.Field
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializerProvider

import java.io.IOException

class MapToListFieldJsonSerializer : JsonSerializer<Map<String, Field>>() {

    private val mapper = ObjectMapper()

    @Throws(IOException::class)
    override fun serialize(value: Map<String, Field>, gen: JsonGenerator, serializers: SerializerProvider) {

        serializers.defaultSerializeValue(value.values, gen)

    }

}
