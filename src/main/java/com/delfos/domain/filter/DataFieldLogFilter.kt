package com.delfos.domain.filter

import com.delfos.domain.DataFieldLog
import org.springframework.data.mongodb.core.index.Indexed

data class DataFieldLogFilter(
        @Indexed
        override var fieldId: String? = null,
        var operator: String? = null,
        @Indexed
        override var value: Any? = null
) : DataFieldLog()
