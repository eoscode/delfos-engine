package com.delfos.domain

import com.eoscode.springapitools.data.domain.Identifier
import org.hibernate.annotations.GenericGenerator
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "action_type_parameter")
data class ActionTypeParameter(
        @Id
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        @GeneratedValue(generator = "uuid")
        @Column(name = "id")
        private var id: String? = null,

        @NotNull
        @Length(min = 2, max = 100)
        @Column(name = "name", length = 100, nullable = false)
        var name: String? = null,

        @Column(name = "description", length = 350)
        var description: String? = null,

        @Column(name = "security", nullable = false)
        var isSecurity: Boolean = false,

        @Column(name = "requerid", nullable = false)
        var isRequired: Boolean = false,

        @NotNull
        @ManyToOne
        @JoinColumn(name = "action_type_id", nullable = false)
        var actionType: ActionType? = null
) : Identifier<String> {

    @Column(name = "action_type_id", insertable = false, updatable = false)
    val actionTypeId: String? = null
        get() = actionType?.id?.let { it } ?: field

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ActionTypeParameter) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

}
