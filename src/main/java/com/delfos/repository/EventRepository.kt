package com.delfos.repository

import com.delfos.domain.Event
import com.eoscode.springapitools.data.repository.Repository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.Optional

@org.springframework.stereotype.Repository
interface EventRepository : Repository<Event, String> {

    @Query("select distinct event from Event as event"
            + " left join event.rules as rule"
            + " where event.status = 1"
            + " and event.state = 1"
            + " and rule.id = :ruleId"
            + " order by event.name")
    fun findByEnabledAndRule(@Param("ruleId") ruleId: String): Optional<List<Event>>

    fun findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCaseOrderByName(name: String, description: String): List<Event>

}
