package com.delfos.service

import com.delfos.domain.Condition
import com.delfos.repository.ConditionRepository
import com.eoscode.springapitools.service.AbstractService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ConditionService : AbstractService<ConditionRepository, Condition, String>() {

    @Autowired
    private val conditionRepository: ConditionRepository? = null

    override fun getRepository(): ConditionRepository? {
        return conditionRepository
    }

}
