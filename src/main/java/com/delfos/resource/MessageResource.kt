package com.delfos.resource

import com.delfos.domain.Message
import com.delfos.service.MessageService
import com.eoscode.springapitools.resource.AbstractResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/messages")
class MessageResource : AbstractResource<MessageService, Message, String>() {

    @Autowired
    private lateinit var messageService: MessageService

    override fun getService(): MessageService {
        return messageService
    }

    @GetMapping("/detail/name/{name}")
    fun findByName(@PathVariable name: String): ResponseEntity<Message> {
        val entity = service.findByName(name)
        return ResponseEntity.ok().body(entity)
    }

    @GetMapping("/search")
    fun search(@RequestParam("name") name: String): ResponseEntity<List<Message>> {
        val list = service.findByNameOrDescription(name)
        return ResponseEntity.ok(list)
    }

}
