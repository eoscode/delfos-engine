package com.delfos.domain.converter

import com.delfos.domain.EventType

import javax.persistence.AttributeConverter

class EventTypeConverter : AttributeConverter<EventType, Int> {

    override fun convertToDatabaseColumn(type: EventType): Int? {
        return type.value
    }

    override fun convertToEntityAttribute(value: Int): EventType {
        return when (value) {
            0 -> EventType.OK
            1 -> EventType.WARNING
            2 -> EventType.INFORMATION
            3 -> EventType.ERROR
            4 -> EventType.FATAL
            else -> EventType.INFORMATION
        }
    }
}
