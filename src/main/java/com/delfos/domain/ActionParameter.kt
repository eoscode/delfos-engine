package com.delfos.domain

import com.eoscode.springapitools.data.domain.Identifier
import org.hibernate.annotations.GenericGenerator
import org.hibernate.validator.constraints.Length

import javax.persistence.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Entity
@Table(name = "action_parameter")
data class ActionParameter(
        @Id
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        @GeneratedValue(generator = "uuid")
        @Column(name = "id", nullable = false)
        private var id: String? = null,

        @NotNull
        @ManyToOne
        @JoinColumn(name = "parameter_id")
        var type: ActionTypeParameter? = null,

        @NotEmpty
        @Length(min = 1, max = 500)
        @Column(name = "value", length = 500, nullable = false)
        var value: String? = null,

        @NotNull
        @ManyToOne
        @JoinColumn(name = "action_id")
        var action: Action? = null
) : Identifier<String> {

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    @Column(name = "parameter_id", insertable = false, updatable = false)
    val typeId: String? = null
        get() = type?.id?.let { it } ?: field

    @Column(name = "action_id", insertable = false, updatable = false)
    val actionId: String? = null
        get() = if (action != null && action?.id != null)
            action!!.id
        else
            field


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ActionParameter) return false

        if (type != other.type) return false

        return true
    }

    override fun hashCode(): Int {
        return type?.hashCode() ?: 0
    }

}
