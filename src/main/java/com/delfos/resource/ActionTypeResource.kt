package com.delfos.resource

import com.delfos.domain.ActionType
import com.delfos.service.ActionTypeService
import com.eoscode.springapitools.resource.AbstractResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/actiontypes")
class ActionTypeResource : AbstractResource<ActionTypeService, ActionType, String>() {

    @Autowired
    private lateinit var actionTypeService: ActionTypeService

    override fun getService(): ActionTypeService {
        return actionTypeService
    }

}
