package com.delfos.domain.filter

import java.util.*

data class MessageLogFilter(
        var messageId: String? = null,
        var componentId: String? = null,
        var identifier: String? = null,
        var dateHour: Date? = null,
        var dateHourEnd: Date? = null,
        var dataFields: List<DataFieldLogFilter> = ArrayList()
)
