package com.delfos.resource

import com.delfos.domain.filter.EventLogStatisticFilter
import com.delfos.domain.view.EventLogStatisticView
import com.delfos.service.EventLogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.query.Param
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

import java.util.Date

@RestController
@RequestMapping("/api/event/logs/statistic")
class EventLogStatisticResource @Autowired
constructor(private val eventLogService: EventLogService) {

    @PostMapping
    fun statistic(@RequestBody statisticBy: EventLogStatisticFilter): ResponseEntity<List<EventLogStatisticView>> {
        val view = eventLogService.statisticQuery(statisticBy)
        return ResponseEntity.ok(view)
    }

    @GetMapping("/date")
    fun statisticByDate(
            @Param("dateHour") @DateTimeFormat(pattern = "yyyy-MM-dd") dateHour: Date,
            @Param("dateHourEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") dateHourEnd: Date,
            @Param("groups") groups: Array<String>): ResponseEntity<List<EventLogStatisticView>> {

        val view = eventLogService.statisticQueryByDate(
                dateHour,
                dateHourEnd,
                groups)

        return ResponseEntity.ok(view)
    }

}
