package com.delfos.engine.rules

import com.delfos.domain.*

class ExpressionBuilder {

    fun build(attribute: String, condition: Condition): String? {
        if (condition is MessageCondition) {
            return build(attribute, condition)
        } else if (condition is MessageDataFieldCondition) {
            return build(attribute, condition)
        }
        return ""
    }

    fun build(attribute: String, condition: MessageCondition): String {
        return if (condition.expressionType == MessageCondition.CONDITION_NAME) {
            String.format("%s(%s, \"%s\")", condition.operator, attribute, condition.value)
        } else " "
    }

    fun build(attribute: String, condition: MessageDataFieldCondition): String? {

        if (condition.operator.equals(OperatorType.IS_NOT_NUL.value, ignoreCase = true)
                || condition.operator.equals(OperatorType.IS_NULL.value, ignoreCase = true)) {

            return attribute + ".get(\"" + condition.field?.name + "\")" + getOperatorExpression(condition.operator!!)

        } else if (condition.field?.type == FieldType.NUMBER) {

            return String.format("%s(%s, \"%s\", \"%s\")", condition.operator, attribute,
                    condition.field?.name, condition.value)

        } else if (condition.field?.type == FieldType.BOOLEAN) {

            return String.format("%s(%s, \"%s\", \"%s\")", condition.operator, attribute,
                    condition.field?.name, condition.value)

        } else if (condition.field?.type == FieldType.STRING) {

            return String.format("%s(%s, \"%s\", \"%s\")", condition.operator, attribute,
                    condition.field?.name, condition.value)
        }
        return null
    }

    fun getOperatorExpression(operator: String): String {

        when (operator) {
            "eq" -> return " == "
            "ne" -> return " != "
            "gt" -> return " > "
            "lt" -> return " < "
            "ge" -> return " >= "
            "le" -> return " <= "
            "isNull" -> return " == null "
            "isNotNull" -> return " != null "
            "and" -> return " && "
            "or" -> return " || "
        }
        return ""

    }

}
